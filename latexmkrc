$ENV{'TZ'} = 'Europe/Madrid';
$xelatex   = "xelatex -synctex=1 -shell-escape %O %S";
$pdf_mode  = 5;
$dvi_mode  = $postscript_mode = 0;
$clean_full_ext = "%R.nav %R.snm %R-dot2tex*.dot %R-dot2tex*.tex %R.vrb dot2tex.log";
