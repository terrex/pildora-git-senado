SHELL = /bin/bash

.PHONY: current clean local-build

current:
	latexmk -pvc main.tex

clean:
	latexmk -C main.tex
	$(RM) dot2tex.log

local-build:
	docker run --rm -it -v $(CURDIR):/project --workdir=/project registry.gitlab.com/terrex/pildora-git-senado:master make all

main-notes.tex: main.tex
	sed -e 's@\\documentclass\[spanish,10pt\]{beamer}@\\documentclass[spanish,10pt,notes]{beamer}@' $? > main-notes.tex

%.pdf: %.tex
	latexmk $?

all: main.pdf main-notes.pdf
