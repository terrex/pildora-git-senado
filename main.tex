%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

%\documentclass[spanish,10pt,notes]{beamer}
\documentclass[spanish,10pt]{beamer}

\mode<presentation>
{
  \usetheme{Madrid}
  \setbeamercovered{transparent}
  \usefonttheme{professionalfonts}
}

\useoutertheme[footline=authorinstitutetitle,subsection=false]{smoothbars}

\usepackage[spanish]{babel}
\usepackage{lipsum}
\usepackage[no-math]{fontspec}
\usepackage[debug]{dot2texi}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,intersections,fit,positioning}
\usepackage{caption}
\usepackage{subcaption}

%% style %%
\defaultfontfeatures{Mapping=tex-text,Numbers={OldStyle}}
\setmainfont[Mapping=tex-text]{TeX Gyre Pagella}
\setromanfont[Mapping=tex-text]{TeX Gyre Pagella}
\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Gill Sans}
\setmonofont[Scale=0.7]{Menlo}

\logo{\includegraphics[width=0.3cm]{logo}}

\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Índice}
    \tableofcontents[currentsection]
  \end{frame}
}

%% custom macros %%
\newcommand{\email}[1]{%
  \href{mailto:#1}{\nolinkurl{<#1>}}}
\newcommand{\rulesep}{\unskip\ \vrule\ }
\newcommand{\code}[1]{\texttt{#1}}

%% doc info %%
\title{Introducción a GIT}
\author{Guillermo Gutiérrez}
\institute{Senado de España}
\date{14 de mayo de 2021}
\subject{Píldora informativa sobre una introducción rápida a GIT}

\hypersetup{
    pdftitle={Introducción a GIT},
    pdfauthor={Guillermo Gutiérrez},
    pdfsubject={Píldora informativa sobre una introducción rápida a GIT},
    pdfkeywords={píldora informativa,píldora,GIT,Senado,Área de Desarrollo,Gelabert,Sistema de Control de Versiones,SCM,VCS},
    pdfstartview=Fit,
    pdfpagelayout=SinglePage,
    pdffitwindow=true,
}


\begin{document}

\frame{\titlepage}

\begin{frame}
\frametitle{Índice}
\tableofcontents
\end{frame}

\section[Introducción]{¿Qué es un Sistema de Control de Versiones?}

% Version Control System (VCS)
% Source Code Management (SCM)

\begin{frame}
\frametitle{Concepto de \emph{Control de versiones}}
\begin{definition}[Control de versiones]
Es un sistema que ayuda en la gestión de los cambios al código fuente de un proyecto.
\end{definition}
Proporciona:
\begin{itemize}
  \item Mecanismo de almacenamiento de los diferentes elementos que se gestionan (\emph{ficheros} y/o \emph{directorios}).
  \item Trazabilidad de las modificaciones parciales a cada uno de ellos (fecha/hora, usuario y comentario).
  \item Mantenimiento del histórico de cambios.
  \item Posibilidad de recuperar el estado de los elementos a un estado anterior o posterior en el histórico.
\end{itemize}
\vspace{1em}
También llamado SCM (\emph{Source Code Management}: gestión de código fuente).\index{SCM}
\end{frame}

\begin{frame}
\frametitle{¿Por qué es necesario un SCM?}
{\centering\includegraphics[width=0.99\textwidth]{img/control-versiones-manual.png}}
\end{frame}
\note{
  Lo primero que hay que preguntarse es: ¿para qué queremos un sistema de control
  de versiones? ¿Es necesario?

  El uso de un sistema de control de versiones facilita la tarea de recuperar
  versiones anteriores y tener perfectamente bajo control cuales son
  las versiones anteriores y cuales las posteriores.

  Lo que queremos es evitar principalmente realizar de manera manual
  copias de ficheros renombrando
  en cada uno la versión o la fecha, así como poder realizar comparaciones
  entre ellos y visualizar fácilmente lo que ha ido cambiando en cada versión.
}

\begin{frame}
\frametitle{Definiciones (I)}
\begin{definition}[Repositorio]
Lugar donde se almacenan los ficheros y cada una de sus modificaciones.
\end{definition}
\begin{definition}[Revisión]
Una \emph{revisión} identifica el estado del conjunto de ficheros en un punto concreto en el tiempo (|| \emph{versión}).
\end{definition}
\begin{definition}[\emph{tag} o etiqueta]
Un \emph{tag} es un nombre que se le asigna a una revisión en particular para localizarla más fácilmente.
\end{definition}
\begin{definition}[\emph{branch} o rama]
Un \emph{branch} es una bifurcación de la linea de desarrollo que puede evolucionar de manera independiente a partir de su creación.
\end{definition}
\end{frame}
\note{
  Empezamos con una serie de nociones básicas y terminología
  común a distintos sistemas de control de versiones.
}

\begin{frame}
\frametitle{Definiciones (II)}
\begin{definition}[\emph{commit}, confirmación o entrega]
Un \emph{commit} es la entrega de un conjunto de modificaciones sobre los ficheros
que forman parte una \emph{revisión} que se crea en el \emph{repositorio}.
\end{definition}
$\hookrightarrow$ Una secuencia de \emph{commit}'s produce una línea de desarrollo (un \emph{branch}).
\begin{definition}[\emph{merge} o fusión]
Un \emph{merge} se realiza para reunificar dos o más ramas que se habían bifurcado volviendo a formar parte de una única línea de desarrollo.
\end{definition}
\begin{definition}[conflicto]
Un \emph{conflicto} surge cuando se realizan dos modificaciones incompatibles sobre las mismas localizaciones de un conjunto de ficheros.
\end{definition}
$\hookrightarrow$ Los conflictos hay que \emph{resolverlos} antes de hacer una entrega (\emph{commit}) al repositorio.
\end{frame}

\section[Centralizados \emph{vs.} distribuidos]{Sistemas \emph{centralizados} vs \emph{distribuidos}}

\begin{frame}
\frametitle{Diferencias entre SCM centralizados vs distribuidos (I)}
\begin{figure}
  \centering
  \includegraphics[height=7cm]{img/SCM Centralizado}
  \caption{Sistema centralizado}
\end{figure}
\end{frame}
\note{
  Vamos a ilustrar la principal diferencia entre los
  sistemas centralizados y distribuidos.

  Como podemos ver en este diagrama, en un sistema
  centralizado existe una única copia del repositorio
  de versiones en un servidor central.

  Desde cada cliente se realiza frecuentemente
  conexiones al servidor para recuperar el proyecto
  en una versión concreta o realizar las entregas.

  Igualmente para crear una rama o etiqueta, al ser
  operaciones sobre el repositorio, se debe hacer
  teniendo conexión con el servidor central.

  Los usuarios sólo poseen en su PC el estado de los
  ficheros del proyecto en una única versión determinada.
}

\begin{frame}
\frametitle{Diferencias entre SCM centralizados vs distribuidos (II)}
\begin{figure}
  \centering
  \includegraphics[height=7cm]{img/SCM Distribuido}
  \caption{Sistema distribuido}
\end{figure}
\end{frame}
\note{
  En el caso de sistemas distribuidos, cada cliente
  puede ser también servidor, y todos tienen una copia
  del repositorio central que sirve de punto de sincronización.

  En los clientes, las actualizaciones a los ficheros se realizan
  hacia y desde el repositorio local, y es este repositorio
  local el que puede sincronizarse con el repositorio central
  o cualquier otro repositorio.
}

\begin{frame}
\frametitle{Diferencias entre SCM centralizados vs distribuidos (y III)}
\begin{columns}
\begin{column}{0.5\textwidth}
En un sistema centralizado:
\begin{itemize}
\item Una única copia del repositorio completo
\item Se require acceso al servidor
\item Las ramas experimentales deben estar en el
servidor central
\end{itemize}
\end{column}
\begin{column}{0.5\textwidth}
En un sistema distribuido:
\begin{itemize}
\item Múltiples copias del repositorio completo
\item Puedes trabajar \emph{offline}
\item Se pueden desarrollar ramas experimentales \emph{efímeras}
\end{itemize}
\end{column}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Características de Subversion}
Lo que ya conocemos de Subversion:
\begin{itemize}
  \item Es un sistema centralizado.
  \item Las revisiones se identifican por un número secuencial.
  \item El protocolo de transporte puede ser WebDAV (a través de http/s) o uno propio de svn.
  \item Permite el bloqueo de archivos (comando \code{svn lock}).
  \item SVN no soporta el manejo de ramas ni tags $\Rightarrow$ el convenio es realizarlo mediante copia de directorios:\\
  \code{svn cp https://.../proy/trunk https://.../proy/tags/proy-1.0.5}
  \item SVN no soporta el renombrado de ficheros. Se realiza como una copia a un fichero nuevo y un posterior borrado del nombre anterior.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Un poco de historia...}
Desde Finlandia para el mundo:
\footnote{Extraído de \url{https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Una-breve-historia-de-Git}}
\begin{itemize}
  \item Linus Torvalds comienza el desarrollo del kernel Linux.
  \item La comunidad de desarrollo de Linux, así como el proyecto, crece.
  \item Las modificaciones al código se realizan mediante parches y traspaso de archivos entre los desarrolladores.
  \item A partir del 2002, empiezan a usar \emph{BitKeeper}, un SCM distribuido de código propietario (no es software libre).
  \item En 2005 la empresa detrás de BitKeeper deja de ofrecerlo de forma gratuita.
  \item Torvalds y sus compañeros inician el desarrollo de un SCM libre basado en la experiencia con BitKeeper y mejorando:
  \begin{itemize}
    \item rendimiento para proyectos grandes
    \item soporte para desarrollo no lineal (con miles de ramas)
  \end{itemize}
\end{itemize}
\end{frame}

\section[Áreas y estructura]{Las distintas áreas y la estructura del repositorio de GIT}

\begin{frame}
\frametitle{Definiciones (y III)}
\begin{definition}[\emph{hunk}]
Conjunto de líneas que cambian en un fichero.
\end{definition}
$\hookrightarrow$ Para encontrar las diferencias entre ficheros se usa el comando \href{https://linux.die.net/man/1/diff}{\code{diff}}.
\begin{definition}[\emph{patch}, parche o \emph{diff}]
Conjunto de \emph{hunks} en uno o más ficheros.
\end{definition}
$\hookrightarrow$ Para aplicar esas diferencias se usa el comando \href{https://linux.die.net/man/1/patch}{\code{patch}}.
\end{frame}

\begin{frame}
\begin{figure}
  \centering
  \includegraphics[height=8cm]{img/diff}
  \caption{Diff \href{https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=b741596468b010af2846b75f5e75a842ce344a6e}{\texttt{b741596468b010af2846b75f5e75a842ce344a6e}} en \texttt{torvalds/linux.git}}
\end{figure}
\end{frame}
\note{
  Esto es un ejemplo de un fichero de diferencias en \emph{formato unificado de diff}
  con un contexto de tres líneas anteriores y posteriores a cada cambio.

  La líneas con prefijo + en verde significa que se añaden,
  y las líneas con prefijo - en rojo significa que se eliminan.

  La línea con doble arroba \code{@@} indica las coordenadas
  para localizar el cambio dentro del fichero antes y después
  de la modificación.

  Gracias a las líneas de contexto, el parche se puede seguir
  aplicando aunque varíen ligeralmente las coordenadas siempre
  que se mantenga el contexto.
}

\begin{frame}
\frametitle{Las distintas áreas GIT}
\begin{figure}
  \centering
  \includegraphics[width=11.5cm]{img/GIT Stages}
  \caption{Áreas de GIT}
\end{figure}
\end{frame}
\note{
  En este diagrama podemos ver lo que se llaman las distintas
  \emph{áreas} o zonas:
  El repositorio remoto es lo único que está en el servidor,
  el resto de zonas pertenecen al PC del usuario.
  Como hemos comentado, el repositorio local es el que se comunica
  con el repositorio remoto, llevando y trayendo objetos
  para sincronizarse en un sentido o en el otro. Pueden haber más
  de una referencia a repositorios remotos. Por convenio, el principal
  se suele llamar el \emph{remote} \code{origin}.

  El usuario en su directorio de trabajo puede hacer entregas
  directamente al repositorio local, o temporalmente a
  un área intermedia (el \emph{staging area}, también
  llamado \emph{index} o \emph{cache}) que va acumulando
  lo que formará parte del próximo \emph{commit}.

  Por otro lado, git proporciona también una zona llamada
  \emph{stash} que se utiliza como una pila LIFO (último
  en entrar, primero en salir) donde almacenar temporalmente
  conjuntos de diferencias. Se suele usar para no perder
  trabajo parcial realizado en un rama que se apila en el
  \emph{stash} antes de cambiar a otra rama en la que
  se quiere consultar o trabajar alternativamente.
}

\begin{frame}[plain]
\begin{figure}
  \centering
  \includegraphics[height=9cm]{img/git-flow}
\end{figure}
\end{frame}
\note{
  Al ser un sistema de control de versiones tan versátil, permite diferentes flujos de
  trabajo: mostramos aquí un ejemplo.
  En este diagrama cada línea vertical representa una rama o línea de desarrollo que avanza
  en el tiempo, y cada circulito representa un \emph{commit} o revisión.

  Este es un posible flujo de trabajo con GIT. En él, se definen dos ramas principales,
  \emph{master} que es la rama con las versiones estables, y
  \emph{develop} para realizar los desarrollos sobre esta otra rama y mantenerlo separado de la
  rama \emph{master} que contendrá únicamente las versiones estables.

  A su vez, la rama de desarrollo puede separarse en otras ramas para la implementación
  de características o correcciones de \emph{bugs}. Por ejemplo, se podría crear un rama
  para la implementación de cada ticket de redmine, ya sea para resolver una incidencia
  o para implementar una funcionalidad nueva. En esta rama se entregarían distintas versiones
  parciales de la resolución del ticket para finalmente fusionarlo con la rama principal de desarrollo
  por ejemplo cuando lo verifique un \emph{revisor} que de su visto bueno.

  También se permite realizar cambios sobre el código de la rama \emph{master}
  pero en una rama separada de \emph{hotfixes} para solucionar de forma urgente
  los errores que afecten a producción.
}

\begin{frame}{Modelo de objetos}
\begin{figure}
  \centering
  \includegraphics[height=7cm]{img/git-objects}
\end{figure}
\end{frame}
\note{
  Ahora os presento un esquema de los tipos de objetos que se
  almacenan en la base de datos del repositorio.
  Como ya hemos comentado, sería igual tanto en el repositorio
  local así como en el repositorio remoto.
  Cada objeto se identifica por el valor de su función resumen
  de su contenido utilizando el algoritmo \code{SHA1}.

  Se guardan objetos de tipo \emph{blob}, que almacena
  los datos ya sean binarios o de texto de los ficheros completos.
  Por otro lado, los objetos de tipo \emph{tree} son tablas
  con los apuntadores a otros blob's de los ficheros, y
  a otros tree's con las subcarpetas que contiene.

  Finalmente, el \emph{commit} apunta a un objeto tree
  con la raíz del proyecto en un instante determinado, incluyendo
  la información del autor, la persona que realiza la confirmación
  (el \emph{commiter}, que normalmente es el mismo), fecha y hora,
  y un mensaje de comentario. Y lo mas importante: la referencia
  a un commit padre del que se derive.

  Se puede referenciar como padres dos o más commits, en cuyo caso
  quiere decir que este commit es un \emph{merge} o fusión de varias ramas.

  Además, se incluye el objeto de tipo \emph{tag} que
  es una manera de referenciar mediante un nombre descriptivo
  cualquier otro objeto: tree, blob, o commit, aunque lo más frecuente
  es ponerle etiquetas a los commits que representen
  versiones finalizadas.
}

\section[Operaciones]{Operaciones con GIT}

\begin{frame}
\frametitle{Operaciones con GIT (I)}
\begin{itemize}
  \item Crear rama local a partir de la actual y cambiar a ella:
\\$\code{git checkout -b nombre} = \code{git branch nombre} + \code{git checkout nombre}$
  \item Seleccionar interactivamente los \emph{hunk}'s a añadir a la caché:
\\\code{git add -p}
  \item Sincronizar rama remota a rama local y fusionar:
\\$\code{git pull} = \code{git fetch} + \code{git merge}$
  \item Iniciar la herramienta de resolución de conflictos (p. ej.: \href{http://meldmerge.org/}{meld})
\\\code{git mergetool}
  \item Realizar una búsqueda dicotómica de la primera revisión que introdujo un error (\emph{biseccionar})
  dados una revisión «mala» y una «buena»:
\\\code{git bisect start}
\\\code{git bisect bad}
\\\code{git bisect good proy-1.0.3}
\\(\code{git bisect run script-test} para encontrar automáticamente la primera revisión con errores)
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Operaciones con GIT (II)}
\begin{itemize}
  \item Crear un repositorio vacío:
\\\code{git init}
  \item Clonar un repositorio existente:
\\\code{git clone https://github.com/user/repo.git}
  \item Añadir un \emph{remote}:
\\\code{git remote add origin https://github.com/user/repo.git}
  \item Crear un nuevo commit que sea el inverso de otro:
\\\code{git revert revisión}
  \item Aplicar un commit perteneciente a otra rama, creando un nuevo commit sin realizar merge:
\\\code{git cherry-pick revisión}
  \item Visualizar el registro de cambios, incluyendo los diffs:
\\\code{git log -w -b -p}
  \item Visualizar con anotaciones al margen la revisión y el autor que modificó por última vez
  cada una de las líneas de un fichero:
\\\code{git blame fichero}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Operaciones con GIT (III). Fusiones}
\begin{itemize}
  \item Fusión sin \emph{fast-forward}: puede continuar el desarrollo en la \code{otra-rama}.
\\\code{git merge -{}-no-ff otra-rama}
  \item Fusión normal (con \emph{fast-forward}): la \code{otra-rama} termina.
\\\code{git merge otra-rama}
\end{itemize}
\begin{figure}
  \centering
  \includegraphics[height=6cm]{img/gitlab-merge-ff}
\end{figure}
\end{frame}
\note{
  Pongamos como ejemplo que queremos mezclar la rama actual \code{develop}
  con alguna otra rama de \code{feature} donde se haya ido desarrollando
  una funcionalidad concreta.

  Hay varias formas de realizar las fusiones o mezcla de ramas.

  En la primera de ellas, sin \emph{fast-forward}, se crea un nuevo
  commit con dos padres, y se permite continuar trabajando en la otra rama.

  En la segunda forma, con \emph{fast-forward}, la rama actual
  se apropia de todos los commits de la rama que se está fusionando,
  y se termina dicha rama.
}

\begin{frame}
\frametitle{Operaciones con GIT (IV). \emph{Rebasing}}
\begin{itemize}
  \item Cambio de base, \emph{rebase} (para ramas de uso local):
\\\code{git rebase rama-base}: vuelve a repetir todos los cambios producidos
en la rama actual, sobre la nueva cima de la rama indicada como base.
Atención: modifica el histórico ya que modifica las referencias a los commit's padres.
\end{itemize}
\begin{figure}
  \centering
  \includegraphics[height=6cm]{img/git-rebase}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Operaciones con GIT (V). Combinar \emph{(squash)} y fusionar}
\begin{itemize}
  \item Fusión \emph{squash}: Crea un nuevo commit que es la combinacion de todos los commits
  que están en la otra rama y no en la actual, y lo entrega en la actual:
\\\code{git merge -{}-squash otra-rama}
\end{itemize}
\begin{figure}
  \centering
  \includegraphics[height=6cm]{img/commit-squashing-diagram}
\end{figure}
\end{frame}

\section[Características]{Ventajas e inconvenientes de GIT}

\begin{frame}
\frametitle{Ventajas de inconvenientes de GIT}
Ventajas:
\begin{itemize}
  \item Distribuido: sin punto central de fallo, funciona sin conexión
  \item Rápido y eficiente
  \item La creación y fusión de ramas es rápida
  \item Permite flujos de trabajo flexibles
  \item El \emph{staging area} permite un mayor control sobre lo que
  se va a entregar
\end{itemize}
Inconvenientes:
\begin{itemize}
  \item Es complejo
  \item Curva de aprendizaje elevada
  \item Para la gestión de ficheros binarios muy grandes
  se usa una extensión llamada Git~LFS.
\end{itemize}
También hay ocasiones en que la misma operación se puede realizar con
distintos conjuntos de comandos y parámetros diferentes, con ligeras
variaciones. Esto puede verse como una ventaja (o no).
\end{frame}

\section[Herramientas]{Herramientas alrededor de GIT}

\begin{frame}
\frametitle{Herramientas de escritorio: Sourcetree}
  \centering
  \includegraphics[height=7.5cm]{img/sourcetree}
\end{frame}

\begin{frame}
\frametitle{Herramientas de escritorio: Meld}
  \centering
  \includegraphics[height=7.5cm]{img/meld}
\end{frame}

\begin{frame}
\frametitle{Herramientas de escritorio: gitk}
  \centering
  \includegraphics[height=7.5cm]{img/gitk}
\end{frame}

\begin{frame}
\frametitle{Herramientas de escritorio: plugin de Eclipse}
  \centering
  \includegraphics[height=7.5cm]{img/eclipse}
\end{frame}

\begin{frame}
\frametitle{Herramientas en la nube: github}
  \centering
  \includegraphics[height=7.5cm]{img/github}
\end{frame}

\begin{frame}
\frametitle{Herramientas en la nube: Gitlab}
  \centering
  \includegraphics[height=7.5cm]{img/gitpod-corrigiendo-docker-7}
\end{frame}

\begin{frame}
\frametitle{Herramientas en la nube: gitpod y docker}
  \centering
  \includegraphics[height=7.2cm]{img/gitpod-corrigiendo-docker-4}
\end{frame}

% http://shafiul.github.io/gitbook/1_the_git_object_model.html
% https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Una-breve-historia-de-Git
% https://aulasoftwarelibre.github.io/taller-de-git/
% https://nvie.com/posts/a-successful-git-branching-model/
% https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html
% http://meldmerge.org
% https://gitimmersion.com/index.html
% https://www.campusmvp.es/recursos/post/que-es-git-ventajas-e-inconvenientes-y-por-que-deberias-aprenderlo-bien.aspx

\begin{frame}[fragile]
\frametitle{Referencias}
\begin{itemize}
  \item \url{http://shafiul.github.io/gitbook/1_the_git_object_model.html}
  \item \url{https://git-scm.com/book/es/v2/Inicio---Sobre-el-Control-de-Versiones-Una-breve-historia-de-Git}
  \item \url{https://aulasoftwarelibre.github.io/taller-de-git/}
  \item \url{https://nvie.com/posts/a-successful-git-branching-model/}
  \item \url{https://blog.thoughtram.io/git/2014/11/18/the-anatomy-of-a-git-commit.html}
  \item \url{http://meldmerge.org}
  \item \url{https://gitimmersion.com/index.html}
  \item \url{https://www.campusmvp.es/recursos/post/que-es-git-ventajas-e-inconvenientes-y-por-que-deberias-aprenderlo-bien.aspx}
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Y esto es todo}
\centering
{\Large \emph{Gracias por su atención}}\\
\vspace{0.5cm}
Guillermo Gutiérrez Herrera\\
\vspace{0.2cm}
\email{guillermo.gutierrez@senado.es}\\
\vspace{0.2cm}
\email{desarrollo.gelabert@senado.es}
\end{frame}

\end{document}
